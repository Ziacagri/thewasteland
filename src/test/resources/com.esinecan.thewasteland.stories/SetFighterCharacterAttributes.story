Meta:

Narrative:
I want to manipulate the empty fighter character instance I
pass to a setter and get its common attributes ready depending
type and class.

Scenario: Fighter character set up
Given I have a fighter character
When I set its attributes
Then the fighter character has a type
Then the fighter character has a health
Then the fighter character has a speed
Then the fighter character has a damage
Then the fighter character has 300 golds
Then the fighter character will be attackable