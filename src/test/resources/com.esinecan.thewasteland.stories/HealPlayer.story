Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description
Given I am not at full health
When I am near a PitStop
When I do a PitStop action towards its direction
Then I should have more health
Then I should have less gold
When I am at full health
When I do a PitStop action towards its direction
Then I should have same health
Then I should have same gold
When I am only a little below full health
When I do a PitStop action towards its direction
Then I should have less gold
Then I should have full health