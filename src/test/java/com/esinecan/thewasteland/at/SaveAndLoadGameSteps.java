package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.EnemyCharacter;
import com.esinecan.thewasteland.character.PlayableCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.io.SaveAndLoad;
import gherkin.lexer.Pl;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import java.util.Random;

/**
 * Created by eren on 29.08.2016.
 */
public class SaveAndLoadGameSteps extends Steps {
    private Game game;
    private PlayableCharacter playableCharacter;
    private EnemyCharacter enemyCharacter;
    private PlayableCharacter newPlayableCharacter;
    private EnemyCharacter newEnemyCharacter;
    private int enemyIndex;
    private SaveAndLoad saveAndLoad;

    @Given("I'm playing a game")
    public void givenImPlayingAGame() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
        playableCharacter = game.getPlayableCharacter();
        Random random = new Random();
        enemyIndex = random.nextInt(game.getEnemies().size());
        enemyCharacter = game.getEnemies().get(enemyIndex);
        saveAndLoad = new SaveAndLoad();
    }

    @When("I save")
    public void whenISave() {
        saveAndLoad.save(game);
    }

    @When("I load")
    public void whenILoad() {
        game = saveAndLoad.load();
        newPlayableCharacter = game.getPlayableCharacter();
        newEnemyCharacter = game.getEnemies().get(enemyIndex);
    }

    @Then("my position is the same")
    public void thenMyPositionIsTheSame() {
        assert (playableCharacter.getXPosition()==newPlayableCharacter.getXPosition() && playableCharacter.getYPosition() == newPlayableCharacter.getYPosition());
    }

    @Then("my health is the same")
    public void thenMyHealthIsTheSame() {
        assert (playableCharacter.getHealth()==newPlayableCharacter.getHealth());
    }

    @Then("a randomy selected enemy position is the same")
    public void thenARandomySelectedEnemyPositionIsTheSame() {
        assert (enemyCharacter.getXPosition()==newEnemyCharacter.getXPosition() && enemyCharacter.getYPosition() == newEnemyCharacter.getYPosition());
    }

    @Then("a randomly selected enemy health is the same")
    public void thenARandomlySelectedEnemyHealthIsTheSame() {
        assert (enemyCharacter.getHealth()==newEnemyCharacter.getHealth());
    }
}
