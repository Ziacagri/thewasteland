package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * The superclass for non fighting characters
 * such as quest giver and pit stop owner
 */
public class NonFighterCharacter extends Character{

    /**
     * The constructor for non fighting characters.
     * Since they are not fighting characters, they
     * can't be attacked.
     * @param characterType
     */
    public NonFighterCharacter(CharacterTypes.CharacterType characterType){
        super(characterType);
        this.setAttackable(false);
    }
}
